---
title: My Article
date: 2020-05-27
tags: example
---

Elon Musk is officially a father to a seventh son, and leave it to the tech-savvy CEO of SpaceX and Tesla, Inc. to choose one of the most unique names for his child that you've ever heard of: X Æ A-Xii Musk. Elon and his partner, Canadian pop singer Grimes, welcomed their newborn son on May 4 and shared his full unique name — which was originally X Æ A-12 — on Twitter.

"X, the unknown variable ⚔️," Grimes wrote in a tweet detailing her son's name. "Æ, my elven spelling of Ai (love &/or Artificial intelligence) . . . A-12 = precursor to SR-17 (our favorite aircraft). No weapons, no defenses, just speed. Great in battle, but non-violent . . . (A=Archangel, my favorite song) (⚔️🐁 metal rat)." Grimes later shared that her son's name is pronounced "Ecks," just like the letter X and "Aye-eye," just as the two letters A and I sound individually (though Elon shared on the Joe Rogan podcast that the Æ is pronounced "Ash." *shrugs*).

There's a lot to unpack there, but essentially, Elon and Grimes seem to have combined many of their loves and interests — along with a nod to 2020 being the year of the Metal Rat according to Chinese zodiac — into the most unique name we've ever seen.